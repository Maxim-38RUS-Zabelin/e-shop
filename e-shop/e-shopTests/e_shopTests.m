//
//  e_shopTests.m
//  e-shopTests
//

#import "eProduct.h"
#import "eShop.h"

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

@interface e_shopTests : XCTestCase

@end

@implementation e_shopTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)addProducts:(NSArray *)products
              error:(NSError *__autoreleasing *)error
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    void (^completion)(NSError *) =
        ^(NSError *eShopError) {
            (*error) = eShopError;

            dispatch_semaphore_signal(semaphore);
        };

    [[eShop defaultShop] addProducts:products
                          completion:completion];

    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

- (NSUInteger)countAvailableProductsAndReturnError:(NSError *__autoreleasing *)error
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    NSUInteger __block numberOfProducts = 0;
    eShopRequest *const request = [[eShopRequest alloc] init];
    request.success = ^(NSNumber *count) {
        numberOfProducts = [count unsignedIntegerValue];

        dispatch_semaphore_signal(semaphore);
    };
    request.failure = ^(NSError *eShopError) {
        (*error) = eShopError;

        dispatch_semaphore_signal(semaphore);
    };
    [[eShop defaultShop] countForRequest:request];

    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

    return numberOfProducts;
}

- (NSArray *)fetchAvailableProductsAndReturnError:(NSError *__autoreleasing *)error
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    NSArray *__block availableProducts = nil;
    eShopRequest *const request = [[eShopRequest alloc] init];
    request.success = ^(NSArray *fetchedProducts) {
        availableProducts = fetchedProducts;

        dispatch_semaphore_signal(semaphore);
    };
    request.failure = ^(NSError *eShopError) {
        (*error) = eShopError;

        dispatch_semaphore_signal(semaphore);
    };
    request.propertiesToOrderBy = @[@"name"];
    [[eShop defaultShop] executeRequest:request];

    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

    return availableProducts;
}

- (void)testProducts
{
    NSString *name = @"iPhone 6";
    NSString *details =
        @"iPhone 6 isn’t simply bigger — it’s better in every way. "
         "Larger, yet dramatically thinner. More powerful, but remarkably "
         "power efficient. With a smooth metal surface that seamlessly meets "
         "the new Retina HD display. It’s one continuous form where hardware "
         "and software function in perfect unison, creating a new generation "
         "of iPhone that’s better by any measure.";
    NSNumber *price = @(199);
    eProductState state = eProductStateDefault;

    eProduct *const product =
        [[eProduct alloc] initWithName:name
                               details:details
                                 price:price
                                 state:state];

    XCTAssertEqualObjects(product.name, name, @"Failed to create a product (invalid name).");
    XCTAssertEqualObjects(product.details, details, @"Failed to create a product (invalid details).");
    XCTAssertEqualObjects(product.price, price, @"Failed to create a product (invalid price).");
    XCTAssertEqual(product.state, state, @"Failed to create a product (invalid state).");

    eMutableProduct *const mutableProduct = [product mutableCopy];
    XCTAssertEqualObjects(product.name, mutableProduct.name, @"Failed to create a copy of the original product (invalid name).");
    XCTAssertEqualObjects(product.details, mutableProduct.details, @"Failed to create a copy of the original product (invalid details).");
    XCTAssertEqualObjects(product.price, mutableProduct.price, @"Failed to create a copy of the original product (invalid price).");
    XCTAssertEqual(product.state, mutableProduct.state, @"Failed to create a copy of the original product (invalid state).");

    name = @"iPad Air 2";
    details = @"So capable, you won’t want to put it down. So thin and light, you won’t have to.";
    price = @(499);
    state = eProductStateIsPurchased;

    mutableProduct.name = name;
    XCTAssertEqualObjects(mutableProduct.name, name, @"Failed to update the copy of the original product (invalid name).");
    mutableProduct.details = details;
    XCTAssertEqualObjects(mutableProduct.details, details, @"Failed to update the copy of the original product (invalid details).");
    mutableProduct.price = price;
    XCTAssertEqualObjects(mutableProduct.price, price, @"Failed to update the copy of the original product (invalid price).");
    mutableProduct.state = state;
    XCTAssertEqual(mutableProduct.state, state, @"Failed to update the copy of the original product (invalid state).");

    NSError *__autoreleasing error = nil;
    NSArray *const products = @[[mutableProduct copy], product];
    [self addProducts:products
                error:&error];
    XCTAssertNil(error, @"Failed to put a couple of products on the list of available products.");

    NSUInteger const numberOfAvailableProducts = [self countAvailableProductsAndReturnError:&error];
    XCTAssertNil(error, @"Failed to count the products available at the e-shop.");
    XCTAssertEqual(numberOfAvailableProducts, [products count],
                   "The number of original products differs from the number of products available at the e-shop.");

    NSArray *const availableProducts = [self fetchAvailableProductsAndReturnError:&error];
    XCTAssertNil(error, @"Failed to fetch the products available at the e-shop.");
    XCTAssertNotNil(availableProducts, @"The fetched products appear to be corrupted.");
    XCTAssertEqual([availableProducts count], [products count],
                   "The number of original products differs from the number of products available at the e-shop.");

    for (NSUInteger index = 0; index < numberOfAvailableProducts; ++index) {
        eProduct *const originalProduct = [products objectAtIndex:index];
        eProduct *const availableProduct = [availableProducts objectAtIndex:index];

        XCTAssertEqualObjects(originalProduct.name, availableProduct.name,
                              @"The original product differs from the one available at the e-shop (names are different).");
        XCTAssertEqualObjects(originalProduct.details, availableProduct.details,
                              @"The original product differs from the one available at the e-shop (details are different).");
        XCTAssertEqualObjects(originalProduct.price, availableProduct.price,
                              @"The original product differs from the one available at the e-shop (prices are different).");
    }
}

@end
