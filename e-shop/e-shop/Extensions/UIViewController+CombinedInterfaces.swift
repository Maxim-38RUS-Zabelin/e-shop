//
//  UIViewController+CombinedInterfaces.swift
//  e-shop
//

import UIKit

public extension UIViewController {

    /**
    
    Embeds the content of the receiving view controller inside of a new navigation stack.

    */
    func embedInNavigationController() -> UINavigationController {
        return UINavigationController(rootViewController: self);
    }
}
