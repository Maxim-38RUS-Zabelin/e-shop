//
//  AppDelegate.m
//  e-shop
//

#import "AppDelegate.h"
#import "NSDictionary+SyntacticSugar.h"
#import "eProduct.h"
#import "eShop.h"
#import "eShopViewController.h"
#import "e_shop-Swift.h"

@interface AppDelegate ()

+ (NSArray */* of eProducts */)defaultProducts;

@end

@implementation AppDelegate

#pragma mark - Class methods

+ (NSArray *)defaultProducts
{
#define kName @"Name"
#define kDetails @"Details"
#define kPrice @"Price"

    NSURL *const url = [[NSBundle mainBundle] URLForResource:@"DefaultProducts"
                                               withExtension:@"plist"];
    NSAssert((url != nil), @"The default set of products is unavailable.");

    NSArray *const rawProducts = [NSArray arrayWithContentsOfURL:url];
    NSAssert((rawProducts != nil), @"The default set of products is malformed.");
    NSAssert(([rawProducts count] > 0), @"The default set of products appears to be empty.");

    eMutableProduct *mutableProduct = nil;
    NSMutableArray *const defaultProducts = [NSMutableArray arrayWithCapacity:[rawProducts count]];
    for (id rawProduct in rawProducts) {
        NSAssert(([rawProduct isKindOfClass:[NSDictionary class]]), @"One of the default products is malformed.");
        @autoreleasepool {
            NSString *const name = [rawProduct stringForKey:kName];
            NSAssert((name != nil), @"One of the default products is malformed (name appears to be invalid).");
            mutableProduct = [[eMutableProduct alloc] initWithName:name];

            mutableProduct.details = [rawProduct stringForKey:kDetails];
            mutableProduct.price = [rawProduct numberForKey:kPrice];

            [defaultProducts addObject:[mutableProduct copy]];
        }
    }

    return [defaultProducts copy];

#undef kName
#undef kDetails
#undef kPrice
}

#pragma mark - UIApplicationDelegate implementation

-           (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    eShop *const defaultShop = [eShop defaultShop];
    eShopViewController *const shopViewController = [[eShopViewController alloc] init];
    shopViewController.shop = defaultShop;

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [shopViewController embedInNavigationController];
    self.window.backgroundColor = [UIColor whiteColor];

    [self.window makeKeyAndVisible];

    NSError *__autoreleasing error = nil;
    if (![[defaultShop url] checkResourceIsReachableAndReturnError:&error]) {
        NSAssert((([error.domain isEqualToString:NSCocoaErrorDomain]) && (error.code == NSFileReadNoSuchFileError)),
                 @"An unexpected error has occured while checking whether the default e-shop is up and running.");

        NSArray *const defaultProducts = [[self class] defaultProducts];
        void (^completion)(NSError *) =
            ^(NSError *error) {
                NSAssert((error == nil),
                         @"Failed to put the default set of products on the list of products available at the default e-shop.");
            };
        [defaultShop addProducts:defaultProducts
                      completion:completion];
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /// The implementation is intentionally left blank.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /// The implementation is intentionally left blank.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /// The implementation is intentionally left blank.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /// The implementation is intentionally left blank.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /// The implementation is intentionally left blank.
}

@end
