//
//  NSURL+UserDomainDirectories.m
//  e-shop
//

#import "NSURL+UserDomainDirectories.h"

@implementation NSURL (UserDomainDirectories)

#pragma mark - Class methods

+ (NSArray *)URLsForDirectoryInUserDomain:(NSSearchPathDirectory)directory
{
    return [[NSFileManager defaultManager] URLsForDirectory:directory
                                                  inDomains:NSUserDomainMask];
}

+ (instancetype)applicationSupportDirectoryURL
{
    return [[self URLsForDirectoryInUserDomain:NSApplicationSupportDirectory] lastObject];
}

+ (instancetype)cachesDirectoryURL
{
    return [[self URLsForDirectoryInUserDomain:NSCachesDirectory] lastObject];
}

+ (instancetype)documentsDirectoryURL
{
    return [[self URLsForDirectoryInUserDomain:NSDocumentDirectory] lastObject];
}

@end
