//
//  NSDictionary+SyntacticSugar.h
//  e-shop
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SyntacticSugar)

/**
 *  @brief  Returns an @b NSNumber object associated with a specified key. If the specified key is
 *          associated with an object of other class, the method returns @c nil.
 */
- (NSNumber *)numberForKey:(NSString *)key;

/**
 *  @brief  Returns an @b NSString object associated with a specified key. If the specified key is
 *          associated with an object of other class, the method returns @c nil.
 */
- (NSString *)stringForKey:(NSString *)key;

/**
 *  @brief  Returns an @b NSValue object associated with a specified key. If the specified key is
 *          associated with an object of other class, the method returns @c nil.
 */
- (NSValue *)nsvalueForKey:(NSString *)key;

@end
