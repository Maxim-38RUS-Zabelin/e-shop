//
//  NSURL+UserDomainDirectories.h
//  e-shop
//

#import <Foundation/Foundation.h>

@interface NSURL (UserDomainDirectories)

/**
 *  @brief  Returns the location of application support files (plug-ins, etc).
 */
+ (instancetype)applicationSupportDirectoryURL;

/**
 *  @brief  Returns the location of discardable cache files.
 */
+ (instancetype)cachesDirectoryURL;

/**
 *  @brief  Returns the location of user's documents.
 */
+ (instancetype)documentsDirectoryURL;

@end
