//
//  NSNumberFormatter+ePrice.h
//  e-shop
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (ePrice)

/**
 *  @brief  Creates and returns an @b NSNumberFormatter object suitable for formatting of
 *          @b eProduct's prices.
 */
+ (instancetype)ePriceFormatter;

@end
