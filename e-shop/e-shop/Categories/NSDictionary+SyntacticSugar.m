//
//  NSDictionary+SyntacticSugar.m
//  e-shop
//

#import "NSDictionary+SyntacticSugar.h"

@interface NSDictionary (PrivateSyntacticSugar)

- (id)objectOfClass:(Class)desirableClass
             forKey:(NSString *)key;

@end

@implementation NSDictionary (SyntacticSugar)

- (id)objectOfClass:(Class)desirableClass
             forKey:(NSString *)key
{
    id object = [self objectForKey:key];
    if ([object isKindOfClass:desirableClass]) {
        return object;
    }

    return nil;
}

- (NSNumber *)numberForKey:(NSString *)key
{
    return (NSNumber *)[self objectOfClass:[NSNumber class]
                                    forKey:key];
}

- (NSString *)stringForKey:(NSString *)key
{
    return (NSString *)[self objectOfClass:[NSString class]
                                    forKey:key];
}

- (NSValue *)nsvalueForKey:(NSString *)key
{
    return (NSValue *)[self objectOfClass:[NSValue class]
                                   forKey:key];
}

@end
