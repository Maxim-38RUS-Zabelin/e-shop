//
//  NSNumberFormatter+ePrice.m
//  e-shop
//

#import "NSNumberFormatter+ePrice.h"

@implementation NSNumberFormatter (ePrice)

#pragma mark - Class methods

+ (instancetype)ePriceFormatter
{
    static dispatch_once_t onceToken = 0;
    static NSNumberFormatter *priceFormatter = nil;
    dispatch_once(&onceToken, ^{
        priceFormatter = [[NSNumberFormatter alloc] init];
        priceFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
        priceFormatter.positiveFormat = @"#,##0 ¤";
    });

    return priceFormatter;
}

@end
