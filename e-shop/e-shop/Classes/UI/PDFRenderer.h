//
//  PDFRenderer.h
//  e-shop
//

#import <Foundation/Foundation.h>

extern NSString *const PDFRendererErrorDomain;

enum PDFRendererErrorCodes : NSInteger {
    PDFRendererFailedToCreateAttributedString   = -1,
    PDFRendererFailedToCreateFramesetter        = -2
};

/**
 *  @brief  The @b PDFRenderer class defines a programmatic interface for rendering PDF files.
 */
@interface PDFRenderer : NSObject

/**
 *  @brief  Creates a new PDF file at a specified location using a given piece of text.
 *
 *  @param[in]      URL     The URL specifying a location where a new PDF file will be created.
 *  @param[in]      text    The text to render.
 *  @param[in,out]  error   If a problem occurs and an error parameter is provided, an @b NSError
 *                          object describing the problem will be returned by reference.
 */
+ (void)createPDFFileAt:(NSURL *)URL
              usingText:(NSString *)text
                  error:(NSError *__autoreleasing *)error;

@end
