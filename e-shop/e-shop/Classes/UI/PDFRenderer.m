//
//  PDFRenderer.m
//  e-shop
//

#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>

#import "PDFRenderer.h"

NSString *const PDFRendererErrorDomain = @"com.pdf_renderer.error_domain";

#define GetPageSize()       (CGSize){612.f, 792.f}
#define GetPageFrame()      (CGRect){CGPointZero, GetPageSize()}
#define GetPageMargins()    (UIEdgeInsets){72.f, 72.f, 72.f, 72.f}

@interface PDFRenderer ()

/**
 *  @brief  Renders the next page in the current context.
 *
 *  @param[in]  range       The range pointing to the beginning of the next page.
 *  @param[in]  framesetter The framesetter laying out as much text as will fit into the frame.
 */
+ (CFRange)renderTextInRange:(CFRange)range
            usingFramesetter:(CTFramesetterRef)framesetter;

@end

@implementation PDFRenderer

#pragma mark - Private methods

+ (CFRange)renderTextInRange:(CFRange)range
            usingFramesetter:(CTFramesetterRef)framesetter
{
    CGContextRef currentContext = UIGraphicsGetCurrentContext();

    CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);

    CGRect const frameRect = UIEdgeInsetsInsetRect(GetPageFrame(), GetPageMargins());
    CGMutablePathRef framePath = CGPathCreateMutable();
    CGPathAddRect(framePath, NULL, frameRect);

    CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, range, framePath, NULL);
    CGPathRelease(framePath);

    CGContextTranslateCTM(currentContext, 0, GetPageSize().height);
    CGContextScaleCTM(currentContext, 1.0, -1.0);

    CTFrameDraw(frameRef, currentContext);

    range = CTFrameGetVisibleStringRange(frameRef);
    range.location += range.length;
    range.length = 0;

    CFRelease(frameRef);
    
    return range;
}

#pragma mark - Public methods

+ (void)createPDFFileAt:(NSURL *)URL
              usingText:(NSString *)text
                  error:(NSError *__autoreleasing *)error
{
    NSAssert([URL isFileURL], @"The specified URL is invalid.");
    NSAssert((text != nil), @"The specified text is invalid.");

    CFAttributedStringRef attributedText = CFAttributedStringCreate(NULL, (CFStringRef)text, NULL);
    if (attributedText) {
        NSUInteger const textLength = CFAttributedStringGetLength((CFAttributedStringRef)attributedText);
        CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedText);
        if (framesetter) {
            UIGraphicsBeginPDFContextToFile([URL path], CGRectZero, nil);

            CGRect const pageFrame = GetPageFrame();
            CFRange range = CFRangeMake(0, 0);
            BOOL done = NO;

            do {
                UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
                range = [self renderTextInRange:range usingFramesetter:framesetter];
                if (range.location == textLength) {
                    done = YES;
                }
            } while (!done);

            UIGraphicsEndPDFContext();
            CFRelease(framesetter);
        }
        else {
            if (!!error) {
                NSString *const localizedDescription =
                    @"Could not create the framesetter needed to lay out the atrributed string.";
                (*error) = [NSError errorWithDomain:PDFRendererErrorDomain
                                               code:PDFRendererFailedToCreateFramesetter
                                           userInfo:@{NSLocalizedDescriptionKey: localizedDescription}];
            }
        }
        CFRelease(attributedText);
    }
    else {
        if (!!error) {
            NSString *const localizedDescription =
                @"Could not create an attributed string for the framesetter.";
            (*error) = [NSError errorWithDomain:PDFRendererErrorDomain
                                           code:PDFRendererFailedToCreateAttributedString
                                       userInfo:@{NSLocalizedDescriptionKey: localizedDescription}];
        }
    }
}

@end

#undef GetPageSize
#undef GetPageFrame
#undef GetPageMargins
