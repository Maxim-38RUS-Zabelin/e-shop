//
//  PaginalTableViewController.h
//  e-shop
//

#import "PaginalViewController.h"

/**
 *  @brief  The @b PaginalTableViewController provides a programmatic interface for displaying any
 *          collections of objects in page by page format by using a table view.
 */
@interface PaginalTableViewController : PaginalViewController
/**
 *  @remarks    The UITableViewDataSource, UITableViewDelegate are implemented
 *              publicly only to let the derivatives customize their behaviour.
 *              In other words this declaration is here for protected use only.
 */
    <UITableViewDataSource, UITableViewDelegate>

/**
 *  @brief  Points to the table view managed by the receiver.
 */
@property (nonatomic, weak) IBOutlet UITableView *tableView;

/**
 *  @brief  Notifies the receiver that it's table view's data is obsolete and needs reloading.
 */
- (void)setNeedsTableViewReload;

@end
