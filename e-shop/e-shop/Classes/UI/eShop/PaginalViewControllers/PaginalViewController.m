//
//  PaginalViewController.m
//  e-shop
//

#import "PaginalViewController.h"

#define GetDelayBeforeObjectsReload()   ((NSTimeInterval)0.3)

@interface PaginalViewController ()

/**
 *  @brief  Says whether a fetch is being performed.
 */
@property (nonatomic, getter=isFetching) BOOL fetching;

/**
 *  @brief  Says whether receiver's objects are obsolete and need reloading.
 */
@property (nonatomic, readwrite) BOOL needsObjectsReload;

/**
 *  @brief  Keeps the number of objects that are already fetched at the moment.
 */
@property (nonatomic, readwrite) NSUInteger numberOfFetchedObjects;

/**
 *  @brief  Directly reloads recever's objects.
 */
- (void)reloadObjects;

@end

@implementation PaginalViewController

@synthesize fetching = fetching_;

#pragma mark - Getters

- (BOOL)isFetching
{
    return fetching_;
}

#pragma mark - Template methods

- (NSUInteger)batchSize
{
    return 0;
}

- (BOOL)hasMoreObjectsToFetch
{
    return NO;
}

- (void)fetchObjectsInRange:(NSRange)range
                 completion:(void (^)(NSError *))completion
{
}

- (void)willFetchObjectsInRange:(NSRange)range
{
    self.fetching = YES;
}

- (void)didFetchObjectsInRange:(NSRange)range
{
    self.numberOfFetchedObjects += range.length;
    self.fetching = NO;
}

- (void)didFailToFetchObjectsInRange:(NSRange)range
                               error:(NSError *)error
{
    self.fetching = NO;
}

- (void)dropObjects
{
    self.numberOfFetchedObjects = 0;
}

#pragma mark - Public methods

- (NSRange)rangeOfNextBatchOfObjects
{
    return NSMakeRange(self.numberOfFetchedObjects, [self batchSize]);
}

- (void)fetchNextBatchOfObjects
{
    if (![self isFetching] && [self hasMoreObjectsToFetch]) {
        NSRange const range = [self rangeOfNextBatchOfObjects];
        [self willFetchObjectsInRange:range];

        __typeof(self) __weak weakSelf = self;
        void (^completion)(NSError *) =
            ^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    (error == nil) ?
                        [weakSelf didFetchObjectsInRange:range] :
                        [weakSelf didFailToFetchObjectsInRange:range
                                                         error:error];
                });
            };
        [self fetchObjectsInRange:range
                       completion:completion];
    }
}

- (void)setNeedsObjectsReload
{
    if ([self isViewLoaded] && (self.view.window != nil)) {
        [[self class] cancelPreviousPerformRequestsWithTarget:self
                                                     selector:@selector(reloadObjects)
                                                       object:nil];
        [self performSelector:@selector(reloadObjects)
                   withObject:nil
                   afterDelay:GetDelayBeforeObjectsReload()];
    }
    else {
        self.needsObjectsReload = YES;
    }
}

#pragma mark - Private methods

- (void)reloadObjects
{
    self.needsObjectsReload = NO;
    self.fetching = NO;

    [self dropObjects];
    [self fetchNextBatchOfObjects];
}

#pragma mark - View's lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if (self.needsObjectsReload) {
        [self reloadObjects];
    }
}

@end

#undef GetDelayBeforeObjectsReload
