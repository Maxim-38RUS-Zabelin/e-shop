//
//  PaginalTableViewController.m
//  e-shop
//

#import "PaginalTableViewController.h"

#define GetDelayBeforeTableViewReload() ((NSTimeInterval)0.1)
#define GetHeightForTableFooterView()   64.f

#pragma mark - Details_FooterView

@interface Details_FooterView : UIView

@property (nonatomic, readonly, weak) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation Details_FooterView

@synthesize activityIndicatorView = activityIndicatorView_;

#pragma mark - Getters

- (UIActivityIndicatorView *)activityIndicatorView
{
    if (!activityIndicatorView_) {
        UIActivityIndicatorView *const activityIndicatorView =
            [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicatorView.hidesWhenStopped = YES;
        [activityIndicatorView startAnimating];
        [self addSubview:activityIndicatorView];
        activityIndicatorView_ = activityIndicatorView;
    }

    return activityIndicatorView_;
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];

    self.activityIndicatorView.center = (CGPoint){
        floorf(self.bounds.size.width / 2.f),
        floorf(self.bounds.size.height / 2.f),
    };
}

@end

#pragma mark - PaginalTableViewController

@interface PaginalTableViewController ()

/**
 *  @brief  Says whether receiver's table view's data is obsolete and needs reloading.
 */
@property (nonatomic, readwrite) BOOL needsTableViewReload;

/**
 *  @brief  Creates and returns a pre-defined view to display as a table footer.
 */
+ (UIView *)viewForTableFooter;

/**
 *  @brief  Directly reloads receiver's table view's data.
 */
- (void)reloadTableView;

@end

@implementation PaginalTableViewController

#pragma mark - Class methods

+ (UIView *)viewForTableFooter
{
    CGSize const size = (CGSize){0.f, GetHeightForTableFooterView()};
    CGRect const frame = (CGRect){CGPointZero, size};

    return [[Details_FooterView alloc] initWithFrame:frame];
}

#pragma mark - Public methods

- (void)setNeedsTableViewReload
{
    if ([self isViewLoaded] && (self.view.window != nil)) {
        [[self class] cancelPreviousPerformRequestsWithTarget:self
                                                     selector:@selector(reloadTableView)
                                                       object:nil];
        [self performSelector:@selector(reloadTableView)
                   withObject:nil
                   afterDelay:GetDelayBeforeTableViewReload()];
    }
    else {
        self.needsTableViewReload = YES;
    }
}

#pragma mark - Private methods

- (void)reloadTableView
{
    self.needsTableViewReload = NO;
    [self.tableView reloadData];
}

#pragma mark - View's lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if (self.needsTableViewReload) {
        [self reloadTableView];
    }
}

#pragma mark - Template methods

- (void)willFetchObjectsInRange:(NSRange)range
{
    [super willFetchObjectsInRange:range];

    if (range.location == 0) {
        [self reloadTableView];
    }
    self.tableView.tableFooterView = [[self class] viewForTableFooter];
}

- (void)didFetchObjectsInRange:(NSRange)range
{
    [super didFetchObjectsInRange:range];

    self.tableView.tableFooterView = nil;
}

- (void)didFailToFetchObjectsInRange:(NSRange)range
                               error:(NSError *)error
{
    [super didFailToFetchObjectsInRange:range
                                  error:error];

    self.tableView.tableFooterView = nil;
}

#pragma mark - UITableViewDataSource implementation

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UITableViewDelegate implementation

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger const numberOfSections = [tableView numberOfSections];
    if (indexPath.section == (numberOfSections - 1)) {
        NSInteger const numberOfRowsInSection = [tableView numberOfRowsInSection:indexPath.section];
        if (indexPath.row == (numberOfRowsInSection - 1)) {
            [self fetchNextBatchOfObjects];
        }
    }
}

@end

#undef GetDelayBeforeTableViewReload
#undef GetHeightForTableFooterView
