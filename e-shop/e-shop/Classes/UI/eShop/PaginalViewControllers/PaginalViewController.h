//
//  PaginalViewController.h
//  e-shop
//

#import <UIKit/UIKit.h>

/**
 *  @brief  The @b PaginalViewController provides a programmatic interface for displaying any
 *          collections of objects in page by page format.
 */
@interface PaginalViewController : UIViewController

/**
 *  @brief      Returns the maximum number of objects a particular page can comprise.
 *
 *  @remarks    This is a template method designed for protected use only; that means the derivatives
 *              should override this method to provide a custom number of objects a particular page
 *              can comprise.
 *
 *              The default implementation returns 0.
 */
- (NSUInteger)batchSize;

/**
 *  @brief      Defines whether there is at least one page available for fetching.
 *
 *  @details    This is a template method designed for protected use only; that means the derivatives
 *              should override this method to let the caller know if there is at least one page
 *              available for fetching.
 *
 *              The default implementation returns @c NO.
 */
- (BOOL)hasMoreObjectsToFetch;

/**
 *  @brief      Fetches a batch of objects in a specified range.
 *
 *  @details    This is a template method designed for protected use only; that means the derivatives
 *              must override this method to fetch a batch of objects in the specified range.
 *
 *              Overriding, one must execute the specified @c completion block once a fetch is complete.
 *
 *              The default implementation does nothing.
 *
 *  @param[in]  range       The range of objects to fetch.
 *  @param[in]  completion  The block to execute once a fetch is complete.
 */
- (void)fetchObjectsInRange:(NSRange)range
                 completion:(void (^)(NSError *))completion;

/**
 *  @brief      Notifies the receiver that the next page is about to be fetched.
 *
 *  @details    This is a template method designed for protected use only; that means the derivatives
 *              may override this method to perform custom tasks dedicated to the forthcoming fetch.
 *
 *              The default implementation does nothing; yet, overriding, one must call @c super at
 *              some point in one's implementation.
 *
 *  @param[in]  range   The range of objects to fetch.
 *
 *  @remarks    By default the method is invoked on the main queue.
 */
- (void)willFetchObjectsInRange:(NSRange)range;

/**
 *  @brief      Notifies the receiver that the next page has been succesfully fetched.
 *
 *  @details    This is a template method designed for protected use only; that means the derivatives
 *              may override this method to display the fetched objects in a proper way.
 *
 *              The default implementation does nothing; yet, overriding, one must call @c super at
 *              some point in one's implementation.
 *
 *  @param[in]  range   The range of fetched objects.
 *
 *  @remarks    By default the method is invoked on the main queue.
 */
- (void)didFetchObjectsInRange:(NSRange)range;

/**
 *  @brief      Notifies the receiver that an error has occurred while the next page was fetched.
 *
 *  @details    This is a template method designed for protected use only; that means the derivatives
 *              may override this method to display an error message.
 *
 *              The default implementation does nothing; yet, overriding, one must call @c super at
 *              some point in one's implementation.
 *
 *  @param[in]  range   The range of objects to fetch.
 *  @param[in]  error   An @b NSError object describing the problem that led to a failure.
 *
 *  @remarks    By default the method is invoked on the main queue.
 */
- (void)didFailToFetchObjectsInRange:(NSRange)range
                               error:(NSError *)error;

/**
 *  @brief      Drops all the fetched objects.
 *
 *  @details    This method is designed for protected use only; that means one is allowed to invoke
 *              it only from within this class and its derivatives.
 */
- (void)dropObjects;

/**
 *  @brief  Returns the range of objects to fetch next.
 */
- (NSRange)rangeOfNextBatchOfObjects;

/**
 *  @brief      Fetches the next page of objects.
 *
 *  @details    This method is designed for protected use only; that means one is allowed to invoke 
 *              it only from within this class and its derivatives.
 */
- (void)fetchNextBatchOfObjects;

/**
 *  @brief  Notifies the receiver that its objects are obsolete and need reloading.
 */
- (void)setNeedsObjectsReload;

@end
