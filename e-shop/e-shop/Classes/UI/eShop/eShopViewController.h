//
//  eShopViewController.h
//  e-shop
//

#import "PaginalTableViewController.h"

@class eShop;

/**
 *  @brief  The @b eShopViewController provides a programmatic interface for displaying the list of
 *          products available at a particualr e-shop.
 */
@interface eShopViewController : PaginalTableViewController

/**
 *  @brief  Points to the e-shop represented by the receiver.
 */
@property (nonatomic, strong) eShop *shop;

@end
