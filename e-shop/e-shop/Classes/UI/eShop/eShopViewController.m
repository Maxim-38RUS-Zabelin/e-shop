//
//  eShopViewController.m
//  e-shop
//

#import "e_shop-Swift.h"
#import "eProduct.h"
#import "eProductCell.h"
#import "eProductCreationViewController.h"
#import "eProductViewController.h"
#import "eShop.h"
#import "eShopViewController.h"
#import "Messenger.h"
#import "NSNumberFormatter+ePrice.h"

#define GetClassForTableViewCell()\
    [eProductCell class]
#define GetReuseIdentifierForTableViewCell()\
    NSStringFromClass(GetClassForTableViewCell())

#define GetHeightForSingleRow()   (48.f)

@interface eShopViewController ()
    <eProductCreationViewControllerDelegate>

/**
 *  @brief  Keeps the maximum number of objects a particular page can comprise.
 */
@property (nonatomic, readwrite) NSUInteger batchSize;

/**
 *  @brief  The number of products available at receiver's e-shop.
 */
@property (nonatomic, readwrite) NSUInteger numberOfAvailableProducts;

/**
 *  @brief  Keeps the products that are already fetched from e-shop's internal database.
 */
@property (nonatomic, strong) NSMutableArray *availableProducts;

/**
 *  @brief  Points to the view indicating putting of a new product on the list of products
 *          available at the e-shop.
 */
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicatorView;

/**
 *  @brief      Fetches a batch of products in a specified range.
 *
 *  @param[in]  range       The range of products to fetch.
 *  @param[in]  completion  The block to execute once a fetch is complete.
 */
- (void)fetchAvailableProductsInRange:(NSRange)range
                           completion:(void (^)(NSError *))completion;

/**
 *  @brief  Notifies the receiver that the user wants to add a new product.
 */
- (void)userDidTouchAddBarButtonItem:(id)sender;

/**
 *  @brief  Notifies the receiver that it's e-shop has updated the list of available products.
 */
- (void)eShopDidUpdateListOfAvailableProducts:(NSNotification *)notification;

/**
 *  @brief  Notifies the receiver that it's e-shop has updated the state of a particular product.
 */
- (void)eShopDidUpdateProductState:(NSNotification *)notification;

@end

@implementation eShopViewController

@synthesize shop = shop_;

@synthesize batchSize = batchSize_;
@synthesize availableProducts = availableProducts_;

#pragma mark - Setters

- (void)setShop:(eShop *)shop
{
    if (!!shop_) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:eShopDidUpdateListOfAvailableProductsNotification
                                                      object:shop_];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:eShopDidUpdateProductStateNotification
                                                      object:shop_];
    }
    shop_ = shop;
    if (!!shop_) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(eShopDidUpdateListOfAvailableProducts:)
                                                     name:eShopDidUpdateListOfAvailableProductsNotification
                                                   object:shop_];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(eShopDidUpdateProductState:)
                                                     name:eShopDidUpdateProductStateNotification
                                                   object:shop_];
    }
    [self setNeedsObjectsReload];
}

#pragma mark - Deallocation

- (void)dealloc
{
    self.shop = nil;
}

#pragma mark - View's lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"eShop.Title",);

    UIActivityIndicatorView *const activityIndicatorView =
        [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView hidesWhenStopped];

    self.navigationItem.leftBarButtonItem =
        [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];
    self.activityIndicatorView = activityIndicatorView;

    self.navigationItem.rightBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                      target:self
                                                      action:@selector(userDidTouchAddBarButtonItem:)];

    [self.tableView registerClass:GetClassForTableViewCell()
           forCellReuseIdentifier:GetReuseIdentifierForTableViewCell()];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    if (self.batchSize == 0) {
        CGRect const insetRect = UIEdgeInsetsInsetRect(self.tableView.bounds, self.tableView.contentInset);
        self.batchSize = (NSUInteger)ceilf(insetRect.size.height / GetHeightForSingleRow());
    }
}

#pragma mark - Private methods

- (void)fetchAvailableProductsInRange:(NSRange)range
                           completion:(void (^)(NSError *))completion
{
    if (!completion) {
        completion = ^(NSError *error) {
            /// The implementation is intentionally left blank.
        };
    }

    __typeof(self) __weak weakSelf = self;
    eShopRequest *const request = [[eShopRequest alloc] init];
    request.fetchOffset = range.location;
    request.fetchLimit = range.length;
    request.success = ^(NSArray *fetchedProducts) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.availableProducts addObjectsFromArray:fetchedProducts];
            [weakSelf setNeedsTableViewReload];
            completion(nil);
        });
    };
    request.failure = ^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    };

    [self.shop executeRequest:request];
}

#pragma mark - Template methods

- (NSUInteger)batchSize
{
    return batchSize_;
}

- (BOOL)hasMoreObjectsToFetch
{
    if (!!self.availableProducts) {
        return ([self rangeOfNextBatchOfObjects].location < self.numberOfAvailableProducts);
    }

    return YES;
}

- (void)fetchObjectsInRange:(NSRange)range
                 completion:(void (^)(NSError *))completion
{
    if (!completion) {
        completion = ^(NSError *error) {
            /// The implementation is intentionally left blank.
        };
    }

    if (range.location == 0) {
        __typeof(self) __weak weakSelf = self;
        if (!!self.shop) {
            eShopRequest *const request = [[eShopRequest alloc] init];
            request.success = ^(NSNumber *count) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.numberOfAvailableProducts = [count unsignedIntegerValue];
                    weakSelf.availableProducts = [NSMutableArray array];

                    [weakSelf fetchAvailableProductsInRange:range
                                                 completion:completion];
                });
            };
            request.failure = ^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(error);
                });
            };

            [self.shop countForRequest:request];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.availableProducts = [NSMutableArray array];
                [weakSelf setNeedsTableViewReload];
                completion(nil);
            });
        }
    }
    else {
        [self fetchAvailableProductsInRange:range
                                 completion:completion];
    }
}

- (void)dropObjects
{
    [super dropObjects];

    self.numberOfAvailableProducts = 0;
    self.availableProducts = nil;
}

#pragma mark - Actions

- (void)userDidTouchAddBarButtonItem:(id)sender
{
    eProductCreationViewController *const viewController = [[eProductCreationViewController alloc] init];
    viewController.delegate = self;

    [self presentViewController:[viewController embedInNavigationController]
                       animated:YES
                     completion:nil];
}

#pragma mark - Notifications

- (void)eShopDidUpdateListOfAvailableProducts:(NSNotification *)notification
{
    [self setNeedsObjectsReload];
}

- (void)eShopDidUpdateProductState:(NSNotification *)notification
{
    eProduct *const updatedProduct = notification.userInfo[eShopUserInfoKeyProduct];
    BOOL (^test)(eProduct *, NSUInteger, BOOL *) =
        ^BOOL(eProduct *product, NSUInteger index, BOOL *stop) {
            (*stop) = [updatedProduct.name isEqualToString:product.name];

            return (*stop);
        };
    NSUInteger const index = [self.availableProducts indexOfObjectPassingTest:test];
    if (index != NSNotFound) {
        [self.availableProducts replaceObjectAtIndex:index
                                          withObject:updatedProduct];
        [self setNeedsTableViewReload];
    }
    else {
        [self setNeedsObjectsReload];
    }
}

#pragma mark - UITableViewDataSource implementation

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [self.availableProducts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    eProduct *const product = self.availableProducts[indexPath.row];

    eProductCell *const cell = [tableView dequeueReusableCellWithIdentifier:GetReuseIdentifierForTableViewCell()];
    cell.textLabel.text = product.name;

    if (product.state == eProductStateIsAvailable) {
        __typeof(self) __weak weakSelf = self;
        cell.purchaseProductBlock = ^(eProductCell *invoker) {
            NSNumberFormatter *const numberFormatter = [NSNumberFormatter ePriceFormatter];
            NSString *const messageFormat =
                NSLocalizedString(@"Messenger.Alert.Message.Format.PurchaseConfirmation", );
            NSString *const message =
                [NSString stringWithFormat:messageFormat,
                    product.name, [numberFormatter stringFromNumber:product.price]];
            [Messenger displayMessage:message
                            withTitle:NSLocalizedString(@"Messenger.Alert.Title.Confirmation", )
                    acceptButtonTitle:NSLocalizedString(@"Messenger.Alert.Button.Yes", )
                    cancelButtonTitle:NSLocalizedString(@"Messenger.Alert.Button.No", )
                           completion:^(NSInteger buttonIndex) {
                               if (buttonIndex != [Messenger cancelButtonIndex]) {
                                   void (^completion)(NSError *) =
                                       ^(NSError *error) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error == nil) {
                                                   [weakSelf setNeedsTableViewReload];
                                               }
                                               else {
                                                   [Messenger displayErrorMessage:error.localizedDescription];
                                               }
                                           });
                                       };
                                       [weakSelf.shop purchaseProduct:product
                                                           completion:completion];
                               }
                           }];
        };
    }
    else if (product.state == eProductStateIsBeingPurchased) {
        UIActivityIndicatorView *const activityIndicatorView =
            [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicatorView.hidesWhenStopped = YES;
        [activityIndicatorView startAnimating];

        cell.accessoryView = activityIndicatorView;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }

    return cell;
}

#pragma mark - UITableViewDelegate implementation

-    (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return GetHeightForSingleRow();
}

-       (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];

    eProductViewController *const productViewController = [[eProductViewController alloc] init];
    productViewController.product = self.availableProducts[indexPath.row];

    [self.navigationController pushViewController:productViewController
                                         animated:YES];
}

#pragma mark - eProductCreationViewControllerDelegate implementation

- (void)productCreationViewController:(eProductCreationViewController *)sender
                 didFinishWithProduct:(eProduct *)product
{
    static NSUInteger counter = 0;

    __typeof(self) __weak weakSelf = self;
    void (^inserter)() = ^{
        if (!!product) {
            if (counter == 0) {
                [weakSelf.activityIndicatorView startAnimating];
            }
            ++counter;

            void (^completion)(NSError *) =
                ^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        --counter;
                        if (counter == 0) {
                            [weakSelf.activityIndicatorView stopAnimating];
                        }

                        if (!!error) {
                            [Messenger displayErrorMessage:error.localizedDescription];
                        }
                    });
                };
            [weakSelf.shop addProduct:product
                           completion:completion];
        }
    };
    [sender.presentingViewController dismissViewControllerAnimated:YES
                                                        completion:inserter];
}

@end

#undef GetClassForTableViewCell
#undef GetReuseIdentifierForTableViewCell

#undef GetHeightForSingleRow
