//
//  eProductCell.h
//  e-shop
//

#import <UIKit/UIKit.h>

/**
 *  @brief  The @b eProductCell class provides a programmatic interface for displaying general info
 *          on a product available at an e-shop.
 */
@interface eProductCell : UITableViewCell

/**
 *  @brief      Points to the block that is executed every time the user touches the 'Purchase' button.
 *
 *  @details    Assigning a non-null block, one makes the receiver present the 'Purchase' button.
 *
 *  @remarks    The block is always dispatched on the main queue.
 */
@property (nonatomic, copy) void (^purchaseProductBlock)(eProductCell *invoker);

@end
