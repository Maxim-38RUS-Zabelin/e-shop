//
//  eProductCell.m
//  e-shop
//

#import "eProductCell.h"

@interface eProductCell ()

/**
 *  @brief  Notifies the receiver that the user has touched the 'Purchase' button.
 */
- (void)userDidTouchPurchaseButton:(UIButton *)sender;

@end

@implementation eProductCell

@synthesize purchaseProductBlock = purchaseProductBlock_;

#pragma mark - Setters

- (void)setPurchaseProductBlock:(void (^)(eProductCell *))purchaseProductBlock
{
    self.accessoryView = nil;

    purchaseProductBlock_ = [purchaseProductBlock copy];
    if (!!purchaseProductBlock_) {
        UIButton *const purchaseButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [purchaseButton setTitle:NSLocalizedString(@"eShop.eProduct.Cell.Purchase.Button.Title", )
                        forState:UIControlStateNormal];
        [purchaseButton addTarget:self
                           action:@selector(userDidTouchPurchaseButton:)
                 forControlEvents:UIControlEventTouchUpInside];
        [purchaseButton sizeToFit];
        self.accessoryView = purchaseButton;
    }
}

#pragma mark - Actions

- (void)userDidTouchPurchaseButton:(UIButton *)sender
{
    NSAssert((self.purchaseProductBlock != nil), @"The 'purchaseProductBlock' appears to be invalid.");
    self.purchaseProductBlock(self);
}

#pragma mark - Preparing for reuse

- (void)prepareForReuse
{
    [super prepareForReuse];

    self.purchaseProductBlock = nil;
}

@end
