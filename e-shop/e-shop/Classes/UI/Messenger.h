//
//  Messenger.h
//  e-shop
//

#import <Foundation/Foundation.h>

/**
 *  @brief      The @b Messenger class provides a programmatic interface for
 *              displaying alerts (from a pre-defined set) in a convenient way.
 *
 *  @details    The class is designed so that always exactly one instance exists;
 *              thereafter one accesses its features by invoking the public class
 *              methods.
 *              A major role of a @c Messenger object is to regulate representation
 *              of the alerts: a new alert stays undercover until the visible one
 *              is dismissed.
 */
@interface Messenger : NSObject

/**
 *  @brief      Displays an alert with a given text message and title.
 *
 *  @details    The user is able to dismiss the alert by tapping the "OK" button.
 */
+ (void)displayMessage:(NSString *)textMessage
             withTitle:(NSString *)title;

/**
 *  @brief      Displays an alert with a given error message and the default title
 *              for errors.
 *
 *  @details    The user is able to dismiss the alert by tapping the "OK" button.
 */
+ (void)displayErrorMessage:(NSString *)errorMessage;

/**
 *  @brief      Displays an alert with a given text message and title.
 *              
 *  @details    One is able to provide one's title for the 'accept' and 'cancel'
 *              buttons as well as a completion handler; that is a block invoked
 *              once an alert is dismissed.
 */
+ (void)displayMessage:(NSString *)textMessage
             withTitle:(NSString *)title
     acceptButtonTitle:(NSString *)acceptButtonTitle
     cancelButtonTitle:(NSString *)cancelButtonTitle
            completion:(void (^)(NSInteger buttonIndex))completion;

/**
 *  @brief  Returns the index of the 'Cancel' button.
 */
+ (NSInteger)cancelButtonIndex;

/**
 *  @brief  Returns the default title for the 'Cancel' button.
 */
+ (NSString *)cancelButtonTitle;

@end
