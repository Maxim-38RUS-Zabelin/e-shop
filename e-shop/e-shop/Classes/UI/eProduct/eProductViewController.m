//
//  eProductViewController.m
//  e-shop
//

#import <MessageUI/MessageUI.h>

#import "eProduct.h"
#import "eProductViewController.h"
#import "Messenger.h"
#import "NSNumberFormatter+ePrice.h"
#import "NSURL+UserDomainDirectories.h"
#import "PDFRenderer.h"

#define GetDelayBeforeContentViewReload() ((NSTimeInterval)0.1)

@interface eProductViewController ()
    <MFMailComposeViewControllerDelegate>

/**
 *  @brief  Says whether receiver's content view's data is obsolete and needs reloading.
 */
@property (nonatomic, readwrite) BOOL needsContentViewReload;

/**
 *  @brief  Points to the label compirising the name of the represented product.
 */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

/**
 *  @brief  Points to the label compirising the price of the represented product.
 */
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

/**
 *  @brief  Points to the text view compirising detailed info on the represented product.
 */
@property (weak, nonatomic) IBOutlet UITextView *detailsTextView;

/**
 *  @brief  Notifies the receiver that the user has tapped 'Compose'-button.
 */
- (void)userDidTouchComposeBarButtonItem:(id)sender;

/**
 *  @brief  Notifies the receiver that it's content view's data is obsolete and needs reloading.
 */
- (void)setNeedsContentViewReload;

/**
 *  @brief  Directly reloads receiver's content view.
 */
- (void)reloadContentView;

@end

@implementation eProductViewController

@synthesize product = product_;

#pragma mark - Setters

- (void)setProduct:(eProduct *)product
{
    product_ = product;
    [self setNeedsContentViewReload];
}

#pragma mark - View's lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = NSLocalizedString(@"eProduct.Title",);

    if ([MFMailComposeViewController canSendMail]) {
        self.navigationItem.rightBarButtonItem =
            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                          target:self
                                                          action:@selector(userDidTouchComposeBarButtonItem:)];
    }

    [self setNeedsContentViewReload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (self.needsContentViewReload) {
        [self reloadContentView];
    }
}

#pragma mark - Actions

- (void)userDidTouchComposeBarButtonItem:(id)sender
{
    NSString *const fileName = [self.product.name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL = [[NSURL cachesDirectoryURL] URLByAppendingPathComponent:fileName];
    URL = [URL URLByAppendingPathExtension:@"pdf"];

    NSString *const text = self.product.details;

    __typeof(self) __weak weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, kNilOptions), ^{
        NSError *__autoreleasing autoreleasingError = nil;
        [PDFRenderer createPDFFileAt:URL
                           usingText:text
                               error:&autoreleasingError];

        NSError *const error = [autoreleasingError copy];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error == nil) {
                NSString *const mimeType = @"application/pdf";
                NSString *const fileName = [[URL lastPathComponent] stringByDeletingPathExtension];
                NSData *const payload = [NSData dataWithContentsOfURL:URL];

                MFMailComposeViewController *const mailComposeViewController = [[MFMailComposeViewController alloc] init];
                mailComposeViewController.mailComposeDelegate = weakSelf;
                [mailComposeViewController setSubject:weakSelf.product.name];
                [mailComposeViewController addAttachmentData:payload
                                                    mimeType:mimeType
                                                    fileName:fileName];

                [weakSelf presentViewController:mailComposeViewController
                                       animated:YES
                                     completion:nil];
            }
            else {
                [Messenger displayErrorMessage:error.localizedDescription];
            }
        });
    });
}

#pragma mark -

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [controller.presentingViewController dismissViewControllerAnimated:YES
                                                            completion:nil];
}

#pragma mark - Private methods

- (void)setNeedsContentViewReload
{
    if ([self isViewLoaded] && (self.view.window != nil)) {
        [[self class] cancelPreviousPerformRequestsWithTarget:self
                                                     selector:@selector(reloadContentView)
                                                       object:nil];
        [self performSelector:@selector(reloadContentView)
                   withObject:nil
                   afterDelay:GetDelayBeforeContentViewReload()];
    }
    else {
        self.needsContentViewReload = YES;
    }
}

- (void)reloadContentView
{
    self.needsContentViewReload = NO;

    self.nameLabel.text = self.product.name;
    self.detailsTextView.text = self.product.details;

    if (!!self.product.price) {
        NSNumberFormatter *const numberFormatter = [NSNumberFormatter ePriceFormatter];
        self.priceLabel.text = [numberFormatter stringFromNumber:self.product.price];
    }
    else {
        self.priceLabel.text = nil;
    }
}

@end

#undef GetDelayBeforeContentViewReload
