//
//  eProductCreationViewController.h
//  e-shop
//

#import <UIKit/UIKit.h>

@class eProduct;
@class eProductCreationViewController;

/**
 *  @brief  The protocol describing the duties the instances of the @c eProductCreationViewController
 *          are able to delegate.
 */
@protocol eProductCreationViewControllerDelegate <NSObject>

/**
 *  Notifies the receiver that the sender has finished its job.
 *
 *  @param[in]  sender  The object that notifies the receiver.
 *  @param[in]  product The product the user wants to put on the list of products available at an
 *                      e-shop. If the user has tapped 'Cancel', the @c product is @c nil.
 */
- (void)productCreationViewController:(eProductCreationViewController *)sender
                 didFinishWithProduct:(eProduct *)product;

@end

/**
 *  @brief  The @b eProductCreationViewController provides a programmatic interface for putting new
 *          products on the list of products available at a particualr e-shop.
 */
@interface eProductCreationViewController : UIViewController

/**
 *  @brief  Returns the object that handles the delegated duties.
 */
@property (nonatomic, weak) id<eProductCreationViewControllerDelegate> delegate;

@end
