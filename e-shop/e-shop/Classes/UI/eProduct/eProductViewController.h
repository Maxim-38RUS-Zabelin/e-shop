//
//  eProductViewController.h
//  e-shop
//

#import <UIKit/UIKit.h>

@class eProduct;

/**
 *  @brief  The @b eProductViewController provides a programmatic interface for presenting a
 *          product from the list of products available at a particualr e-shop.
 */
@interface eProductViewController : UIViewController

/**
 *  @brief  Points to the represented project.
 */
@property (nonatomic, strong) eProduct *product;

@end
