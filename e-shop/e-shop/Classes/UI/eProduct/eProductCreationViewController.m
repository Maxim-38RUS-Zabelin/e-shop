//
//  eProductCreationViewController.m
//  e-shop
//

#import "eProduct.h"
#import "eProductCreationViewController.h"
#import "Messenger.h"
#import "NSDictionary+SyntacticSugar.h"
#import "NSNumberFormatter+ePrice.h"

#define GetCornerRadiusForDetailsTextView() (5.f)

#define GetBorderColorForDetailsTextView()  [[UIColor lightGrayColor] CGColor]
#define GetBorderWidthForDetailsTextView()  (.5f)

typedef enum : NSInteger {
    TextFieldTagName    = 314,
    TextFieldTagPrice   = 31415,
} TextFieldTags;

@interface eProductCreationViewController ()
    <UITextFieldDelegate, UITextViewDelegate>

/**
 *  @brief  Keeps info related to the originated product.
 */
@property (nonatomic, strong) eMutableProduct *mutableProduct;

/**
 *  @brief  Points to the text field compirising the name of the originated product.
 */
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

/**
 *  @brief  Points to the text field compirising the price of the originated product.
 */
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;

/**
 *  @brief  Points to the label compirising the currency symbol associated with system's locale.
 */
@property (weak, nonatomic) IBOutlet UILabel *currencySymbolLabel;

/**
 *  @brief  Points to the label compirising a hint of what the user is expected to enter in the
 *          @c detailsTextView.
 */
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;

/**
 *  @brief  Points to the text view compirising detailed info on the originated product.
 */
@property (weak, nonatomic) IBOutlet UITextView *detailsTextView;

/**
 *  @brief  Returns the singleton @c NSNumberFormatter object that is used to format the price
 *          of the originated product.
 */
+ (NSNumberFormatter *)priceFormatter;

/**
 *  @brief  Notifies the receiver that the user has tapped 'Cancel'-button.
 */
- (void)userDidTouchCancelBarButtonItem:(id)sender;

/**
 *  @brief  Notifies the receiver that the user has tapped 'Save'-button.
 */
- (void)userDidTouchSaveBarButtonItem:(id)sender;

@end

@interface eProductCreationViewController (KeyboardNotifications)

/**
 *  @brief  Subscribes the receiver for the notifications the keyboard posts.
 */
- (void)subscribeForKeyboardNotifications;

/**
 *  @brief  Unsubscribes the receiver from the notifications the keyboard posts.
 */
- (void)unsubscribeFromKeyboardNotifications;

/**
 *  @brief  Notifies the receiver that the keyboard is about to show up.
 */
- (void)keyboardWillShow:(NSNotification *)notification;

/**
 *  @brief  Notifies the receiver that the keyboard is about to disappear from the screen.
 */
- (void)keyboardWillHide:(NSNotification *)notification;

@end

@implementation eProductCreationViewController (KeyboardNotifications)

- (void)subscribeForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unsubscribeFromKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *const userInfo = notification.userInfo;
    CGRect keyboardEndFrame = CGRectZero;

    NSValue *const value = [userInfo nsvalueForKey:UIKeyboardFrameEndUserInfoKey];
    if (!!value) {
        [value getValue:&keyboardEndFrame];

        UIWindow *const mainWindow = self.view.window;
        keyboardEndFrame = [mainWindow convertRect:keyboardEndFrame
                                            toView: self.view];
        self.detailsTextView.contentInset = (UIEdgeInsets){.0f, .0f, keyboardEndFrame.size.height, .0f};
        self.detailsTextView.scrollIndicatorInsets = self.detailsTextView.contentInset;
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.detailsTextView.contentInset = UIEdgeInsetsZero;
    self.detailsTextView.scrollIndicatorInsets = self.detailsTextView.contentInset;
}

@end

@implementation eProductCreationViewController

@synthesize mutableProduct = mutableProduct_;

@synthesize nameTextField = nameTextField_;
@synthesize priceTextField = priceTextField_;

#pragma mark - Class methods

+ (NSNumberFormatter *)priceFormatter
{
    static NSNumberFormatter *numberFormatter = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        numberFormatter = [[NSNumberFormatter alloc] init];
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    });

    return numberFormatter;
}

#pragma mark - Getters

- (eMutableProduct *)mutableProduct
{
    if (!mutableProduct_) {
        mutableProduct_ =
            [[eMutableProduct alloc] initWithName:@""
                                          details:@""
                                            price:@(0.0)
                                            state:eProductStateDefault];
    }

    return mutableProduct_;
}

#pragma mark - Setters

- (void)setNameTextField:(UITextField *)nameTextField
{
    nameTextField_ = nameTextField;
    nameTextField_.tag = TextFieldTagName;
}

- (void)setPriceTextField:(UITextField *)priceTextField
{
    priceTextField_ = priceTextField;
    priceTextField_.tag = TextFieldTagPrice;
}

#pragma mark - View's lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;

    self.title = NSLocalizedString(@"eProductCreation.Title",);

    self.navigationItem.leftBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                      target:self
                                                      action:@selector(userDidTouchCancelBarButtonItem:)];
    self.navigationItem.rightBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                      target:self
                                                      action:@selector(userDidTouchSaveBarButtonItem:)];

    self.nameTextField.placeholder = NSLocalizedString(@"eProductCreation.Name.TextField.Placeholder", );
    self.priceTextField.placeholder = NSLocalizedString(@"eProductCreation.Price.TextField.Placeholder", );

    self.currencySymbolLabel.text = [[NSNumberFormatter ePriceFormatter] currencySymbol];
    self.detailsLabel.text = NSLocalizedString(@"eProductCreation.Details.Label.Text", );

    self.detailsTextView.layer.cornerRadius = GetCornerRadiusForDetailsTextView();
    self.detailsTextView.layer.borderColor = GetBorderColorForDetailsTextView();
    self.detailsTextView.layer.borderWidth = GetBorderWidthForDetailsTextView();
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self subscribeForKeyboardNotifications];

    self.nameTextField.text = self.mutableProduct.name;
    self.priceTextField.text = [[[self class] priceFormatter] stringFromNumber:self.mutableProduct.price];
    self.detailsTextView.text = self.mutableProduct.details;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self.view endEditing:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    [self unsubscribeFromKeyboardNotifications];
}

#pragma mark - Actions

- (void)userDidTouchCancelBarButtonItem:(id)sender
{
    [self.delegate productCreationViewController:self
                            didFinishWithProduct:nil];
}

- (void)userDidTouchSaveBarButtonItem:(id)sender
{
    if ([self.mutableProduct.name length] > 0) {
        [self.delegate productCreationViewController:self
                                didFinishWithProduct:[self.mutableProduct copy]];
    }
    else {
        [Messenger displayErrorMessage:NSLocalizedString(@"Messenger.Alert.Message.ProductNameIsEmpty", )];
    }
}

#pragma mark - UITextFieldDelegate implementation

-             (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
            replacementString:(NSString *)string
{
    NSString *text = [textField.text stringByReplacingCharactersInRange:range
                                                             withString:string];
    if (textField.tag == TextFieldTagName) {
        self.mutableProduct.name = text;
    }
    else if (textField.tag == TextFieldTagPrice) {
        NSNumberFormatter *const numberFormatter = [[self class] priceFormatter];
        text = [text stringByReplacingOccurrencesOfString:numberFormatter.groupingSeparator
                                               withString:@""];

        BOOL shouldChangeCharacters = YES;
        NSNumber *price = [numberFormatter numberFromString:text];
        if (!price) {
            price = @(0.0);
        }
        NSNumber *const oldPrice = self.mutableProduct.price;
        if ((oldPrice == nil) || ((oldPrice != nil) && (![price isEqual:oldPrice]))) {
            self.mutableProduct.price = price;
            shouldChangeCharacters = NO;
        }
        if ([text length] == 0) {
            shouldChangeCharacters = NO;
        }
        if (!shouldChangeCharacters) {
            textField.text = [numberFormatter stringFromNumber:price];
        }

        return shouldChangeCharacters;
    }

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == TextFieldTagName) {
        [self.priceTextField becomeFirstResponder];
    }
    else if (textField.tag == TextFieldTagPrice) {
        [self.detailsTextView becomeFirstResponder];
    }

    return YES;
}

#pragma mark - UITextViewDelegate implementation

-        (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
        replacementText:(NSString *)text
{
    self.mutableProduct.details =
        [textView.text stringByReplacingCharactersInRange:range
                                               withString:text];

    return YES;
}

@end

#undef GetCornerRadiusForDetailsTextView

#undef GetBorderColorForDetailsTextView
#undef GetBorderWidthForDetailsTextView
