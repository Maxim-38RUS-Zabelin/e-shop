//
//  Messenger.m
//  e-shop
//

#import <UIKit/UIKit.h>

#import "Messenger.h"

typedef enum {
    AlertTypeOK       = 0,
    AlertTypeOKCancel,
} AlertType;

static NSString *const kAlertType           = @"type";
static NSString *const kMessage             = @"message";
static NSString *const kTitle               = @"title";
static NSString *const kAcceptButtonTitle   = @"okTitle";
static NSString *const kCancelButtonTitle   = @"cancelTitle";
static NSString *const kCompletion          = @"completion";

@interface Messenger () <UIAlertViewDelegate>

+ (instancetype)sharedInstance;

/**
 *  @brief  Defines whether an alert is currently visible or not.
 */
@property (nonatomic, readwrite) BOOL showsAlert;

/**
 *  @brief  Keeps alerts that stay undercover until the visible one is dismissed.
 */
@property (nonatomic, strong) NSMutableArray/*of NSDictionary*/*alertQueue;

/**
 *  @brief  Points to the currently visible alert.
 */
@property (nonatomic, strong) NSDictionary *visibleAlert;

/**
 *  @brief  Points to the view presenting the currently visible alert.
 */
@property (nonatomic, strong) UIAlertView *visibleAlertView;

/**
 *  @brief  Displays an alert of any type.
 */
- (void)displayAlert:(NSDictionary *)alert;

/**
 *  @brief  Displays an alert of @c AlertTypeOK type.
 */
- (void)displayOKAlert:(NSDictionary *)alert;

/**
 *  @brief  Displays an alert of @c AlertTypeOKCancel type.
 */
- (void)displayOKCancelAlert:(NSDictionary *)alert;

/**
 *  @brief  Notifies the receiver that an alert is about to become visible.
 */
- (void)willDisplayAlert:(NSDictionary *)alert
                  inView:(UIAlertView *)alertView;

@end


@implementation Messenger

@synthesize alertQueue = alertQueue_;
@synthesize visibleAlert = visibleAlert_;

#pragma mark - Initialization

+ (instancetype)sharedInstance
{
    static Messenger *sharedMessenger_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMessenger_ = [[self alloc] init];
    });

    return sharedMessenger_;
}

- (instancetype)init
{
    if ((self = [super init])) {
        alertQueue_ = [@[] mutableCopy];
        visibleAlert_ = @{};
    }

    return self;
}

#pragma mark - Class methods

+ (NSInteger)cancelButtonIndex
{
    return [[[Messenger sharedInstance] visibleAlertView] cancelButtonIndex];
}

+ (NSString *)cancelButtonTitle
{
    return NSLocalizedString(@"Messenger.Alert.Button.OK",);
}

+ (void)displayMessage:(NSString *)textMessage
             withTitle:(NSString *)title
{
    NSMutableDictionary *const alert = [NSMutableDictionary dictionary];
    [alert setValue:@(AlertTypeOK) forKey:kAlertType];
    [alert setValue:textMessage forKey:kMessage];
    [alert setValue:title forKey:kTitle];

    [[self sharedInstance] displayAlert:[alert copy]];
}

+ (void)displayErrorMessage:(NSString *)errorMessage
{
    [self displayMessage:errorMessage
               withTitle:NSLocalizedString(@"Messenger.Alert.Title.Error",)];
}

+ (void)displayMessage:(NSString *)textMessage
             withTitle:(NSString *)title
     acceptButtonTitle:(NSString *)acceptButtonTitle
     cancelButtonTitle:(NSString *)cancelButtonTitle
            completion:(void(^)(NSInteger buttonIndex))completion
{
    if (!completion) {
        completion = ^(NSInteger buttonIndex) {};
    }

    NSMutableDictionary *const alert = [NSMutableDictionary dictionary];
    [alert setValue:@(AlertTypeOKCancel) forKey:kAlertType];
    [alert setValue:textMessage forKey:kMessage];
    [alert setValue:title forKey:kTitle];
    [alert setValue:acceptButtonTitle forKey:kAcceptButtonTitle];
    [alert setValue:cancelButtonTitle forKey:kCancelButtonTitle];
    [alert setValue:[completion copy] forKey:kCompletion];

    [[self sharedInstance] displayAlert:[alert copy]];
}

#pragma mark - Displaying alerts

- (void)displayAlert:(NSDictionary *)alert
{
    if (!self.showsAlert) {
        AlertType const type = [[alert valueForKey:kAlertType] intValue];
        switch (type) {
            case AlertTypeOK: {
                [self displayOKAlert:alert];
                return;
            }
            case AlertTypeOKCancel: {
                [self displayOKCancelAlert:alert];
                return;
            }
            default: {
                NSAssert(NO, @"The flow must never reach this point.");
                return;
            }
        }
    }
    else {
        [self.alertQueue addObject:alert];
    }
}

#pragma mark - Displaying OK-Cancel alerts

- (void)displayOKCancelAlert:(NSDictionary *)alert
{
#ifdef DEBUG
    AlertType const type = [[alert valueForKey:kAlertType] intValue];
#endif
    NSAssert((type == AlertTypeOKCancel),
             @"Message type is invalid.");

    NSString *const message = [alert valueForKey:kMessage];
    NSString *const title = [alert valueForKey:kTitle];
    NSString *const acceptButtonTitle = [alert valueForKey:kAcceptButtonTitle];
    NSString *const cancelButtonTitle = [alert valueForKey:kCancelButtonTitle];
    UIAlertView *const alertView =
        [[UIAlertView alloc] initWithTitle:title
                                   message:message
                                  delegate:self
                         cancelButtonTitle:cancelButtonTitle
                         otherButtonTitles:acceptButtonTitle, nil];
    [self willDisplayAlert:alert
                    inView:alertView];
    [alertView show];
}

#pragma mark - Displaying simple alerts

- (void)displayOKAlert:(NSDictionary *)alert
{
#ifdef DEBUG
    AlertType const type = [[alert valueForKey:kAlertType] intValue];
#endif
    NSAssert((type == AlertTypeOK),
             @"Message type is invalid.");

    NSString *const title = [alert valueForKey:kTitle];
    NSString *const message = [alert valueForKey:kMessage];
    NSString *const cancelButtonTitle = [[self class] cancelButtonTitle];
    UIAlertView *const alertView =
        [[UIAlertView alloc] initWithTitle:title
                                   message:message
                                  delegate:self
                         cancelButtonTitle:cancelButtonTitle
                         otherButtonTitles:nil];
    [self willDisplayAlert:alert
                    inView:alertView];
    [alertView show];
}

-  (void)willDisplayAlert:(NSDictionary *)alert
                   inView:(UIAlertView *)alertView
{
    self.visibleAlert = [alert mutableCopy];
    self.visibleAlertView = alertView;
    self.showsAlert = YES;
}

#pragma mark - UIAlertViewDelegate implementation

-         (void)alertView:(UIAlertView *)alertView
didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    AlertType const type = [[self.visibleAlert valueForKey:kAlertType] intValue];
    switch (type) {
        case AlertTypeOKCancel: {
            void(^completion)(NSInteger buttonIndex) =
                [self.visibleAlert objectForKey:kCompletion];
            if (!!completion) {
                completion(buttonIndex);
            }
            break;
        }
        case AlertTypeOK: {
            /// The implementation is intentionally left blank.
            break;
        }
        default: {
            NSAssert(NO, @"The flow must never reach this point.");
            return;
        }
    }

    self.showsAlert = NO;
    if ([self.alertQueue count] > 0) {
        NSDictionary *const alert = [self.alertQueue firstObject];
        [self.alertQueue removeObject:alert];
        [self displayAlert:alert];
    }
}

@end
