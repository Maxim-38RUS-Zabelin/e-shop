//
//  eShop.h
//  e-shop
//

#import <Foundation/Foundation.h>

@class eProduct;
@class eShopRequest;

/**
 *  @brief  An @b eShop posts such a notification every time its list of available products is updated.
 *          The notifications are always posted on the main queue.
 */
extern NSString *const eShopDidUpdateListOfAvailableProductsNotification;

/**
 *  @brief  An @b eShop posts such a notification every time a particular product changes its state.
 *          The notifications are always posted on the main queue.
 */
extern NSString *const eShopDidUpdateProductStateNotification;
/**
 *  @brief  The key associated with a product that has just been purchased.
 */
extern NSString *const eShopUserInfoKeyProduct;

/**
 *  @brief  The @b eShop class defines a programmatic interface for interacting with various products.
 */
@interface eShop : NSObject

/**
 *  @brief  Keeps the file URL the receiver was initialized with.
 */
@property (nonatomic, readonly, copy) NSURL *url;

/**
 *  @brief  Returns the singleton @b eShop object.
 */
+ (instancetype)defaultShop;

/**
 *  @brief  Does no initialization; use @c -initWithContentsOfURL: instead.
 */
- (instancetype)init NS_UNAVAILABLE;

/**
 *  @brief  Initializes a newly created instance with the contents of the database at a specified
 *          URL. If no database exists, a new one is created.
 *
 *  @param[in]  url The file URL to initialize with.
 */
- (instancetype)initWithContentsOfURL:(NSURL *)url NS_DESIGNATED_INITIALIZER;

/**
 *  @brief      Asynchronously puts a single product on the list of products available at the shop.
 *
 *  @details    If an identical product is already available at the shop, it is replaced with a new
 *              one.
 *
 *              The @c completion block is always dispatched on a private queue; thus one should
 *              manually switch to the main queue before updating the UI.
 *
 *  @param[in]  product     The product to put on the list of products available at the shop.
 *  @param[in]  completion  The block object to be executed when the operation finishes.
 */
- (void)addProduct:(eProduct *)product
        completion:(void (^)(NSError *error))completion;

/**
 *  @brief      Asynchronously puts an entire bunch of products on the list of products available at
 *              the shop.
 *
 *  @details    If any of the products is already available at the shop, the product from the list
 *              of available products is replaced with a new one.
 *
 *              The @c completion block is always dispatched on a private queue; thus one should
 *              manually switch to the main queue before updating the UI.
 *
 *  @param[in]  products    The array comprising products to put on the list of products available
 *                          at the shop.
 *  @param[in]  completion  The block object to be executed when the operation finishes.
 *
 *  @remarks    The method @b never validates the incoming arrays; it's up to the invoker to
 *              provide a properly assembled bunch of products.
 */
- (void)addProducts:(NSArray */* of eProducts */)products
         completion:(void (^)(NSError *error))completion;

/**
 *  @brief  Asynchronously fetches a list of products available at the shop by using the specified
 *          request.
 *
 *  @param[in]  request The request that specifies the search criteria for the fetch.
 */
- (void)executeRequest:(eShopRequest *)request;

/**
 *  @brief  Returns the number of products a particular request would have returned
 *          if it had been passed to @c -fetchProductsUsingRequest:.
 *
 *  @param[in]  request The request that specifies the search criteria for the fetch.
 */
- (void)countForRequest:(eShopRequest *)request;

/**
 *  @brief  Asynchronously 'purchases' a given product.
 *
 *  @param[in]  product     The product to purchase.
 *  @param[in]  completion  The block object to be executed when the operation finishes.
 */
- (void)purchaseProduct:(eProduct *)product
             completion:(void (^)(NSError *error))completion;

@end

typedef enum : NSUInteger {
    eShopRequestSortOrderAscending  = 0,
    eShopRequestSortOrderDefault    = eShopRequestSortOrderAscending,
    eShopRequestSortOrderDescending = 1
} eShopRequestSortOrder;

/**
 *  @brief  The @b eShopRequest class defines a programmatic interface for fetching various products
 *          available at an @b eShop.
 */
@interface eShopRequest : NSObject

/**
 *  @brief      Keeps the fetch limit.
 *
 *  @details    The fetch limit specifies the maximum number of products that a particular request
 *              should return when executed.
 */
@property (nonatomic, readwrite) NSUInteger fetchLimit;

/**
 *  @brief      Keeps the fetch offset.
 *
 *  @details    This setting allows one to specify an offset at which rows will begin being returned.
 *              The default value is 0.
 */
@property (nonatomic, readwrite) NSUInteger fetchOffset;

/**
 *  @brief      Defines the sort order of the selection of products a particular request is to fetch.
 *
 *  @details    The default value is @c eShopRequestSortOrderAscending.
 */
@property (nonatomic, readwrite) eShopRequestSortOrder sortOrder;

/**
 *  @brief      Keeps the array of strings that indicate how products should be ordered.
 *
 *  @remarks    The specified keypath strings are inserted into the queries as is;
 *              no checks are performed.
 */
@property (nonatomic, copy) NSArray *propertiesToOrderBy;

/**
 *  @brief      Keeps a block object to be executed when the request operation finishes successfully.
 *
 *  @details    The @c success block is always dispatched on a private queue; thus one should manually
 *              switch to the main queue before updating the UI.
 */
@property (nonatomic, copy) void (^success)(id object);

/**
 *  @brief      Keeps a block object to be executed when the request operation finishes unsuccessfully.
 *
 *  @details    The @c failure block is always dispatched on a private queue; thus one should manually
 *              switch to the main queue before updating the UI.
 */
@property (nonatomic, copy) void (^failure)(NSError *error);

@end
