//
//  eProduct.m
//  e-shop
//

#import "eProduct.h"

#pragma mark - eProduct

@interface eProduct ()
{
@protected
    NSString *name_;
    NSString *details_;
    NSNumber *price_;
    eProductState state_;
}

/**
 *  @brief      Initializes a newly created instance by copying various pieces of information on
 *              the specified product.
 *
 *  @remarks    One may treat this method as a copy-constructor.
 */
- (instancetype)initWithProduct:(eProduct *)product;

@end

@implementation eProduct

@synthesize name = name_;
@synthesize details = details_;
@synthesize price = price_;
@synthesize state = state_;

#pragma mark - Initialization

- (instancetype)initWithName:(NSString *)name
{
    return [self initWithName:name
                      details:nil
                        price:nil
                        state:eProductStateDefault];
}

- (instancetype)initWithName:(NSString *)name
                     details:(NSString *)details
                       price:(NSNumber *)price
                       state:(eProductState)state
{
    NSAssert(name != nil, @"The specified name is invalid.");

    if ((self = [super init])) {
        name_ = [name copy];
        details_ = [details copy];
        price_ = [price copy];
        state_ = state;
    }

    return self;
}

- (instancetype)initWithProduct:(eProduct *)product
{
    return [self initWithName:product.name
                      details:product.details
                        price:product.price
                        state:product.state];
}

#pragma mark - NSCopying implementation

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#pragma mark - NSMutableCopying implementation

- (id)mutableCopyWithZone:(NSZone *)zone
{
    return [[eMutableProduct allocWithZone:zone] initWithProduct:self];
}

@end

#pragma mark - eMutableProduct

@implementation eMutableProduct

@dynamic name;
@dynamic details;
@dynamic price;
@dynamic state;

#pragma mark - Setters

- (void)setName:(NSString *)name
{
    NSAssert(name != nil, @"The specified name is invalid.");
    name_ = [name copy];
}

- (void)setDetails:(NSString *)details
{
    details_ = [details copy];
}

- (void)setPrice:(NSNumber *)price
{
    NSAssert(price != nil, @"The specified price is invalid.");
    price_ = [price copy];
}

- (void)setState:(eProductState)state
{
    state_ = state;
}

#pragma mark - NSCopying implementation

- (id)copyWithZone:(NSZone *)zone
{
    return [[eProduct allocWithZone:zone] initWithProduct:self];
}

@end
