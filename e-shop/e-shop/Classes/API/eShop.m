//
//  eShop.m
//  e-shop
//

#import <FMDB/FMDB.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

#import "eProduct.h"
#import "eShop.h"
#import "NSURL+UserDomainDirectories.h"

#pragma mark - eShopRequest+SQLQueries

@interface eShopRequest (SQLQueries)

/**
 *  @brief  Creates and returns an SQLite query with no aggregate function.
 */
- (NSString *)sqlQuery;

/**
 *  @brief  Creates and returns an SQLite query with a specified aggregate function.
 *
 *  @param[in]  aggregateFunction   The name of the desired aggregate function.
 *                                  May be @c nil or an empty string; that makes
 *                                  the receiver apply no aggregate function.
 *
 *  @see    The full list of built-in aggregate functions may found at
 *          http://www.sqlite.org/lang_aggfunc.html.
 */
- (NSString *)sqlQueryWithAggregateFunction:(NSString *)aggregateFunction;

@end

#pragma mark - eShop

NSString *const eShopDidUpdateListOfAvailableProductsNotification = @"eShopDidUpdateListOfAvailableProducts";

NSString *const eShopDidUpdateProductStateNotification = @"eShopDidUpdateProductState";
NSString *const eShopUserInfoKeyProduct = @"eProduct";

@interface eShop ()

/**
 *  @brief  Keeps the serial queue to perform subsidiary operations.
 */
@property (nonatomic, readonly, strong) dispatch_queue_t subsidiaryQueue;

/**
 *  @brief  Keeps the serial queue to perform operations on the internal database associated with
 *          the receiver.
 */
@property (nonatomic, strong) FMDatabaseQueue *dbQueue;

/**
 *  @brief      Creates a new database at the specified URL and returns a serial queue to perform
 *              operations on this database.
 *
 *  @details    If the database already exists, the method just creates and returns a new serial
 *              queue to perform operations on this database.
 *
 *  @param[in]      url     A file URL that specifies the directory and file name to create a database.
 *  @param[in,out]  error   If a problem occurs and an error parameter is provided, an @b NSError
 *                          object describing the problem will be returned by reference.
 */
+ (FMDatabaseQueue *)createDatabaseAtURL:(NSURL *)url
                                   error:(NSError *__autoreleasing*)error;

/**
 *  @brief  Asynchronously gets the serial queue to perform operations on the internal database
 *          associated with the receiver and performs the given block on this queue.
 *
 *  @param[in]  block   The block to execute when the desired serial queue is obtained or a problem
 *                      occurs.
 *
 *  @param[out] dbQueue The desired serial queue or @c nil if a problem occurs.
 *  @param[out] error   If a problem occurs, an @b NSError object describing the problem is passed in.
 */
- (void)obtainDatabaseQueueAndPerformBlock:(void (^)(FMDatabaseQueue *dbQueue, NSError *error))block;

/**
 *  @brief      Asynchronously puts an entire bunch of products on the list of products available at
 *              the shop.
 *
 *  @details    If any of the products is already available at the shop, the product from the list
 *              of available products is replaced with a new one.
 *
 *  @param[in]  products    The array comprising products to put on the list of products available
 *                          at the shop.
 *  @param[in]  completion  The block object to be executed when the operation finishes.
 *
 *  @remarks    The method @b never validates the incoming arrays; it's up to the invoker to
 *              provide a properly assembled bunch of products.
 */
- (void)insertProducts:(NSArray *)products
            completion:(void (^)(NSError *))completion;

@end

@implementation eShop

@synthesize url = url_;
@synthesize subsidiaryQueue = subsidiaryQueue_;
@synthesize dbQueue = dbQueue_;

#pragma mark - Class methods

+ (instancetype)defaultShop
{
    static eShop *defaultShop = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL applicationSupportDirectoryURL];
        url = [url URLByAppendingPathComponent:@"eShop"];
        url = [url URLByAppendingPathExtension:@"dat"];

        defaultShop = [[self alloc] initWithContentsOfURL:url];
    });

    return defaultShop;
}

+ (FMDatabaseQueue *)createDatabaseAtURL:(NSURL *)url
                                   error:(NSError *__autoreleasing*)error
{
    FMDatabaseQueue *dbQueue = nil;
    BOOL const success =
        [[[NSFileManager alloc] init] createDirectoryAtURL:[url URLByDeletingLastPathComponent]
                               withIntermediateDirectories:YES
                                                attributes:nil
                                                     error:error];
    if (success) {
        NSString *const path = [url path];
        if ([path length] > 0) {
            dbQueue = [[FMDatabaseQueue alloc] initWithPath:path];
        }
        if (dbQueue == nil) {
            // TODO: Create a proper error object.
        }
    }

    return dbQueue;
}

#pragma mark - Getters

- (FMDatabaseQueue *)dbQueue
{
    @synchronized(self) {
        return dbQueue_;
    }
}

#pragma mark - Setters

- (void)setDbQueue:(FMDatabaseQueue *)dbQueue
{
    @synchronized(self) {
        [dbQueue_ close];
        dbQueue_ = dbQueue;
    }
}

#pragma mark - Initialization

- (instancetype)initWithContentsOfURL:(NSURL *)url
{
    NSAssert([url isFileURL], @"The specified URL is invalid.");

    if ((self = [super init])) {
        url_ = [url copy];

        NSString *const identifier = [[NSUUID UUID] UUIDString];
        NSString *const label = [NSString stringWithFormat:@"com.eShop.subsidiaryQueue.%@", identifier];
        subsidiaryQueue_ = dispatch_queue_create([label UTF8String], DISPATCH_QUEUE_CONCURRENT);
    }

    return self;
}

#pragma mark - Deallocation

- (void)dealloc
{
    self.dbQueue = nil;
}

#pragma mark - Private methods

- (void)obtainDatabaseQueueAndPerformBlock:(void (^)(FMDatabaseQueue *, NSError *))block
{
    if (!block) {
        block = ^(FMDatabaseQueue *dbQueue, NSError *error) {
            /// The implementation is intentionally left blank.
        };
    }

    __typeof(self) __weak weakSelf = self;
    dispatch_async(self.subsidiaryQueue, ^{
        if (!weakSelf.dbQueue) {
            NSError *__autoreleasing error = nil;
            FMDatabaseQueue *const dbQueue =
                [eShop createDatabaseAtURL:weakSelf.url
                                     error:&error];
            if ((error == nil) && (dbQueue != nil)) {
                [dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                    NSError *lastError = nil;
                    NSString *const statement =
                        @"CREATE TABLE IF NOT EXISTS eProducts ("
                            "name TEXT, "
                            "details TEXT, "
                            "price DOUBLE, "
                            "state INTEGER, "

                            "PRIMARY KEY (name)"
                        ");";
                    if (![db executeStatements:statement]) {
                        lastError = [db lastError];
                        (*rollback) = YES;
                    }
                    dispatch_async(weakSelf.subsidiaryQueue, ^{
                        if (lastError == nil) {
                            weakSelf.dbQueue = dbQueue;
                            block(weakSelf.dbQueue, lastError);
                        }
                        else {
                            block(nil, lastError);
                        }
                    });
                }];
            }
            else {
                block(nil, error);
            }
        }
        else {
            block(weakSelf.dbQueue, nil);
        }
    });
}

- (void)insertProducts:(NSArray *)products
            completion:(void (^)(NSError *))completion
{
    if ([products count] > 0) {
        id (^transform)(eProduct *product) =
            ^id(eProduct *product) {
                return [product copy];
            };
        NSArray *const immutableProducts = [products map:transform];
        void (^block)(FMDatabaseQueue *, NSError *) =
            ^(FMDatabaseQueue *dbQueue, NSError *error) {
                if ((error == nil) && (dbQueue != nil)) {
                    [dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                        NSError *lastError = nil;
                        NSString *const query = @"REPLACE INTO eProducts VALUES (?, ?, ?, ?);";
                        for (eProduct *product in immutableProducts) {
                            BOOL const success =
                                [db executeUpdate:query,
                                    [product name], [product details], [product price], @((NSUInteger)[product state])
                                ];
                            if (!success) {
                                lastError = [db lastError];
                                (*rollback) = YES;

                                break;
                            }
                        }
                        completion(lastError);
                    }];
                }
                else {
                    completion(error);
                }
            };
        [self obtainDatabaseQueueAndPerformBlock:block];
    }
    else {
        completion(nil);
    }
}

#pragma mark - Public methods

- (void)addProduct:(eProduct *)product
        completion:(void (^)(NSError *))completion
{
    NSAssert((product != nil), @"The specified product is invalid.");

    [self addProducts:@[product]
           completion:completion];
}

- (void)addProducts:(NSArray *)products
         completion:(void (^)(NSError *))completion
{
    if (!completion) {
        completion = ^(NSError *error) {
            /// The implementation is intentionally left blank.
        };
    }

    __typeof(self) __weak weakSelf = self;
    completion = ^(NSError *error) {
        dispatch_async(weakSelf.subsidiaryQueue, ^{
            completion(error);

            dispatch_async(dispatch_get_main_queue(), ^{
                NSNotification *const notification =
                    [[NSNotification alloc] initWithName:eShopDidUpdateListOfAvailableProductsNotification
                                                  object:weakSelf
                                                userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotification:notification];
            });
        });
    };

    /// Let's make an artificial delay to meet the specified requirements.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), self.subsidiaryQueue, ^{
        [weakSelf insertProducts:products
                      completion:completion];
    });
}

- (void)executeRequest:(eShopRequest *)request
{
    NSAssert((request != nil), @"The specified request is invalid.");

    if (!request.success) {
        request.success = ^(id object) {
            /// The implementation is intentionally left blank.
        };
    }

    if (!request.failure) {
        request.failure = ^(NSError *error) {
            /// The implementation is intentionally left blank.
        };
    }

    __typeof(self) __weak weakSelf = self;
    void (^block)(FMDatabaseQueue *, NSError *) =
        ^(FMDatabaseQueue *dbQueue, NSError *error) {
            if ((error == nil) && (dbQueue != nil)) {
                [dbQueue inDatabase:^(FMDatabase *db) {
                    NSString *const sqlQuery = [request sqlQuery];

                    eMutableProduct *mutableProduct = nil;
                    NSMutableArray *const fetchedProducts = [NSMutableArray array];

                    FMResultSet *const resultSet = [db executeQuery:sqlQuery];
                    while ([resultSet next]) {
                        @autoreleasepool {
                            NSString *const name = [resultSet stringForColumn:@"name"];
                            NSAssert((name != nil), @"The result set appears to be corrupted.");
                            mutableProduct = [[eMutableProduct alloc] initWithName:name];

                            mutableProduct.details = [resultSet stringForColumn:@"details"];
                            mutableProduct.price = @([resultSet doubleForColumn:@"price"]);
                            mutableProduct.state = (eProductState)[resultSet intForColumn:@"state"];

                            [fetchedProducts addObject:[mutableProduct copy]];
                        }
                    }

                    /// Let's make an artificial delay to meet the specified requirements.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), weakSelf.subsidiaryQueue, ^{
                        request.success([fetchedProducts copy]);
                    });
                }];
            }
            else {
                /// Let's make an artificial delay to meet the specified requirements.
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), weakSelf.subsidiaryQueue, ^{
                    request.failure(error);
                });
            }
        };
    [self obtainDatabaseQueueAndPerformBlock:block];
}

- (void)countForRequest:(eShopRequest *)request
{
    NSAssert((request != nil), @"The specified request is invalid.");

    if (!request.success) {
        request.success = ^(id object) {
            /// The implementation is intentionally left blank.
        };
    }

    if (!request.failure) {
        request.failure = ^(NSError *error) {
            /// The implementation is intentionally left blank.
        };
    }

    __typeof(self) __weak weakSelf = self;
    void (^block)(FMDatabaseQueue *, NSError *) =
        ^(FMDatabaseQueue *dbQueue, NSError *error) {
            if ((error == nil) && (dbQueue != nil)) {
                [dbQueue inDatabase:^(FMDatabase *db) {
                    NSString *const sqlQuery =
                        [request sqlQueryWithAggregateFunction:@"count"];

                    FMResultSet *const resultSet = [db executeQuery:sqlQuery];

                    NSNumber *count = nil;
                    while ([resultSet next]) {
                        NSAssert((count == nil), @"There are too many results in the provided set.");
                        count = @([resultSet unsignedLongLongIntForColumnIndex:0]);
                    }

                    dispatch_async(weakSelf.subsidiaryQueue, ^{
                        request.success(count);
                    });
                }];
            }
            else {
                dispatch_async(weakSelf.subsidiaryQueue, ^{
                    request.failure(error);
                });
            }
        };
    [self obtainDatabaseQueueAndPerformBlock:block];
}

- (void)purchaseProduct:(eProduct *)product
             completion:(void (^)(NSError *))completion
{
    NSAssert((product != nil), @"The specified product is invalid.");

    if (!completion) {
        completion = ^(NSError *error) {
            /// The implementation is intentionally left blank.
        };
    }

    eMutableProduct *const mutableProduct = [product mutableCopy];

    __typeof(self) __weak weakSelf = self;
    completion = ^(NSError *error) {
        dispatch_async(weakSelf.subsidiaryQueue, ^{
            completion(error);

            dispatch_async(dispatch_get_main_queue(), ^{
                NSNotification *const notification =
                    [[NSNotification alloc] initWithName:eShopDidUpdateProductStateNotification
                                                  object:weakSelf
                                                userInfo:@{eShopUserInfoKeyProduct: [mutableProduct copy]}];
                    [[NSNotificationCenter defaultCenter] postNotification:notification];
            });
        });
    };


    mutableProduct.state = eProductStateIsBeingPurchased;
    [self insertProducts:@[mutableProduct]
              completion:^(NSError *error) {
                  if (error == nil) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          NSNotification *const notification =
                              [[NSNotification alloc] initWithName:eShopDidUpdateProductStateNotification
                                                            object:weakSelf
                                                          userInfo:@{eShopUserInfoKeyProduct: [mutableProduct copy]}];
                          [[NSNotificationCenter defaultCenter] postNotification:notification];
                      });

                      /// Let's make an artificial delay to meet the specified requirements.
                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), weakSelf.subsidiaryQueue, ^{
                          mutableProduct.state = eProductStateIsPurchased;
                          [weakSelf insertProducts:@[mutableProduct]
                                        completion:completion];
                      });
                  }
                  else {
                      completion(error);
                  }
              }];
}

@end

#pragma mark - eShopRequest

@implementation eShopRequest

@synthesize sortOrder = sortOrder_;

#pragma mark - Initialization

- (instancetype)init
{
    if ((self = [super init])) {
        sortOrder_ = eShopRequestSortOrderDefault;
    }

    return self;
}

@end

@implementation eShopRequest (SQLQueries)

- (NSString *)sqlQuery
{
    return [self sqlQueryWithAggregateFunction:nil];
}

- (NSString *)sqlQueryWithAggregateFunction:(NSString *)aggregateFunction
{
    NSMutableString *const mutableQuery = [@"SELECT " mutableCopy];
    ([aggregateFunction length] > 0) ?
        [mutableQuery appendFormat:@"%@(*)", aggregateFunction] :
        [mutableQuery appendString:@"name, details, price, state"];
    [mutableQuery appendString:@" FROM eProducts"];

    if ([self.propertiesToOrderBy count] > 0) {
        [mutableQuery appendString:@" ORDER BY "];
        [mutableQuery appendString:[self.propertiesToOrderBy componentsJoinedByString:@", "]];
        if (self.sortOrder == eShopRequestSortOrderDescending) {
            [mutableQuery appendString:@" DESC "];
        }
    }

    if (self.fetchOffset > 0) {
        [mutableQuery appendString:@" LIMIT "];
        [mutableQuery appendString:[@(self.fetchOffset) description]];
        [mutableQuery appendString:@","];
        [mutableQuery appendString:[@(self.fetchLimit) description]];
    }
    else if (self.fetchLimit > 0) {
        [mutableQuery appendString:@" LIMIT "];
        [mutableQuery appendString:[@(self.fetchLimit) description]];
    }
    [mutableQuery appendString:@";"];

    return [mutableQuery copy];
}

@end
