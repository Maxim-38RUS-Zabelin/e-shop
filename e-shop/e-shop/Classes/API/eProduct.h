//
//  eProduct.h
//  e-shop
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    eProductStateIsAvailable        = 0,
    eProductStateDefault            = eProductStateIsAvailable,
    eProductStateIsBeingPurchased   = 1,
    eProductStateIsPurchased        = 2,
} eProductState;

/**
 *  @brief  The @b eProduct class defines a programmatic interface for gathering various pieces of
 *          information on the products available at an @b eShop.
 */
@interface eProduct : NSObject <NSCopying, NSMutableCopying>

/**
 *  @brief   Returns the string of characters uniquely identifying a particular product.
 */
@property (nonatomic, readonly, copy) NSString *name;

/**
 *  @brief  Returns the string of characters providing additional information on a particular
 *          product.
 */
@property (nonatomic, readonly, copy) NSString *details;

/**
 *  @brief  Returns the price of a particular product.
 */
@property (nonatomic, readonly, copy) NSNumber *price;

/**
 *  @brief  Returns the state of a particular product.
 */
@property (nonatomic, readonly) eProductState state;

/**
 *  @brief  Does no initialization; use @c -initWithName:details:price: instead.
 */
- (instancetype)init NS_UNAVAILABLE;

/**
 *  @brief      Initializes a newly created instance with the specified name and price.
 *
 *  @param[in]  name    The string of characters uniquely identifying a particular product.
 *                      May NOT be @c nil.
 */
- (instancetype)initWithName:(NSString *)name;

/**
 *  @brief      Initializes a newly created instance with the specified name, details and price.
 *
 *  @param[in]  name    The string of characters uniquely identifying a particular product.
 *                      May NOT be @c nil.
 *  @param[in]  details The string of characters providing additional information on a particular
 *                      product.
 *  @param[in]  price   The price of a particular product.
 *  @param[in]  state   The state of a particular product.
 *
 *  @details    This is the designated initializer of the class.
 */
- (instancetype)initWithName:(NSString *)name
                     details:(NSString *)details
                       price:(NSNumber *)price
                       state:(eProductState)state NS_DESIGNATED_INITIALIZER;

@end

/**
 *  @brief  The @b eMutableProduct class defines a programmatic interface for creating objects
 *          providing various pieces of information on the products available at an @b eShop.
 */
@interface eMutableProduct : eProduct

/**
 *  @brief  Keeps the string of characters uniquely identifying a particular product.
 */
@property (nonatomic, copy) NSString *name;

/**
 *  @brief  Keeps the string of characters providing additional information on a particular
 *          product.
 */
@property (nonatomic, copy) NSString *details;

/**
 *  @brief  Keeps the price of a particular product.
 */
@property (nonatomic, copy) NSNumber *price;

/**
 *  @brief  Keeps the state of a particular product.
 */
@property (nonatomic, readwrite) eProductState state;

@end
